package com.hellokoding.account.repository;  
import java.sql.ResultSet;  
import java.sql.SQLException;
import java.util.List;  
import org.springframework.jdbc.core.BeanPropertyRowMapper;  
import org.springframework.jdbc.core.JdbcTemplate;  
import org.springframework.jdbc.core.RowMapper;

import com.hellokoding.account.model.Department;
import com.hellokoding.account.model.User;
import com.hellokoding.account.model.Vacation;  
  
public class UserDao {  
JdbcTemplate template;

	public void setTemplate(JdbcTemplate  template) {  
	    this.template = template;  
	}  
 
	public int delete(int id){  
	    String sql="delete from user where id="+id+"";  
	    return template.update(sql);  
	}  
	public User getUserById(int id){  
	    String sql="select * from user where id=?";  
	    return template.queryForObject(sql, new Object[]{id},new BeanPropertyRowMapper<User>(User.class));  
	}  
	
	public List<User> getUsers(){  
	    return template.query("select id,firstName, lastName from user",new RowMapper<User>(){  
	        public User mapRow(ResultSet rs, int row) throws SQLException {  
	            User e=new User();  
	            e.setId(rs.getInt(1));  
	            e.setFirstName(rs.getString(2));  
	            e.setLastName(rs.getString(3));  
	            return e;  
	        }  
	    });  
	}
	
	public List<Department> getDepartments(){  
	    return template.query("select id,departmentName from department",new RowMapper<Department>(){  
	        public Department mapRow(ResultSet rs, int row) throws SQLException {  
	            Department department = new Department (); 
	            department.setId(rs.getInt(1)); 
	            department.setDepartmentName(rs.getString(2)); 
	            return department;  
	        }  
	    });  
	}
	
	public List<Vacation> getVacationTypes(){  
	    return template.query("select id,vacationType from vacation",new RowMapper<Vacation>(){  
	        public Vacation mapRow(ResultSet rs, int row) throws SQLException {  
	            Vacation vacationType = new Vacation (); 
	            vacationType.setId(rs.getInt(1)); 
	            vacationType.setVacationType(rs.getString(2)); 
	            return vacationType;  
	        }  
	    });  
	}



}  
