package com.hellokoding.account.repository;  
import java.sql.ResultSet;  
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.BeanPropertyRowMapper;  
import org.springframework.jdbc.core.JdbcTemplate;  
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.hellokoding.account.model.Department;
import com.hellokoding.account.model.User;
import com.hellokoding.account.model.UserVacation;
import com.hellokoding.account.model.Vacation;  
  
public class VacationDao {  
JdbcTemplate template;

	VacationDao dao;
  
	public void setTemplate(JdbcTemplate template) {  
	    this.template = template;  
	}
	
	
	public void saveVacation(Vacation p){
		int status = p.isVacationStatus() ? 1 : 0;
		MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
		mapSqlParameterSource.addValue("vacationType", p.getVacationType());
		mapSqlParameterSource.addValue("vacationStart", p.getVacationStart());
		mapSqlParameterSource.addValue("vacationEnd", p.getVacationEnd());   
	    mapSqlParameterSource.addValue("vacationStatus", status);
	    mapSqlParameterSource.addValue("userId", p.getUserId());
	 	   
	    NamedParameterJdbcTemplate jdbcTemplateObject = new 
		         NamedParameterJdbcTemplate(template.getDataSource());
	    
		String sql = "insert into vacation(vacationType, vacationStart, vacationEnd, vacationStatus, userId) "
				   + "VALUES (:vacationType, :vacationStart, :vacationEnd, :vacationStatus, :userId)";
	  
	    jdbcTemplateObject.update(sql, mapSqlParameterSource);  
	    
	    if (status == 1) {
	    	UpdateUserBalance(p.getUserId(), p.getVacationStart(), p.getVacationEnd());
	    }
	}
	
	private int UpdateUserBalance(long l, Date vacationStart, Date vacationEnd) {
		
		int diffDays = GetDiffDays(vacationStart, vacationEnd);
		 
		MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();

	    mapSqlParameterSource.addValue("userId", l);
	    mapSqlParameterSource.addValue("diffDays", diffDays+1);
	 	   
	    NamedParameterJdbcTemplate jdbcTemplateObject = new 
		         NamedParameterJdbcTemplate(template.getDataSource());
	    
		String sql = "UPDATE user "
				   + "SET vacBalance = vacBalance - :diffDays " 
				   + "WHERE id = :userId";
	  
	    return jdbcTemplateObject.update(sql, mapSqlParameterSource);  
	}
	
	private int GetDiffDays(Date vacationStart, Date vacationEnd) {
		Calendar calendar1 = Calendar.getInstance();
		calendar1.setTime(vacationStart);
		long millis1 = calendar1.getTimeInMillis();
		 
		Calendar calendar2 = Calendar.getInstance();
		calendar2.setTime(vacationEnd);
		long millis2 = calendar2.getTimeInMillis();
		 
		long diff = millis2 - millis1;
		 
		int diffDays = (int)(diff / (24 * 60 * 60 * 1000));
		
		return diffDays;
	}

	  
	public Vacation getVacationById(int id){  
	    String sql="select * from vacation where id=?";  
	    return template.queryForObject(sql, new Object[]{id},new BeanPropertyRowMapper<Vacation>(Vacation.class));  
	}
	
	public int updateVacation(Vacation p){
		int status = p.isVacationStatus() ? 1 : 0;
		MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
		mapSqlParameterSource.addValue("id", p.getId());
		mapSqlParameterSource.addValue("vacationType", p.getVacationType());
		mapSqlParameterSource.addValue("vacationStart", p.getVacationStart());
		mapSqlParameterSource.addValue("vacationEnd", p.getVacationEnd());   
	    mapSqlParameterSource.addValue("vacationStatus", status);
	    mapSqlParameterSource.addValue("userId", p.getUserId());
	 	   
	    NamedParameterJdbcTemplate jdbcTemplateObject = new 
		         NamedParameterJdbcTemplate(template.getDataSource());
	    
		String sql = "UPDATE vacation SET vacationType = :vacationType, vacationStart = :vacationStart,"
				+ " vacationEnd = :vacationEnd, vacationStatus = :vacationStatus,"
				+ " userId = :userId WHERE id = :id";

		
		
		return jdbcTemplateObject.update(sql, mapSqlParameterSource);  
	}
	
	public int delete(int id){
		  String sql="delete from vacation where id="+id+"";
		  return template.update(sql);
	}
	
	public List<Vacation> getVacation(int departmentId){
		  return template.query("SELECT v.* FROM department AS d JOIN user AS u ON d.id = u.departmentId\r\n" + 
		  		" JOIN vacation AS v ON u.id = v.userId\r\n" + 
		  		" WHERE departmentId = " + departmentId + " AND vacationStart >= NOW() + INTERVAL 7 DAY ORDER BY vacationStart", new RowMapper<Vacation>(){
		      public Vacation mapRow(ResultSet rs, int row) throws SQLException {
		          Vacation e = new Vacation();
		          e.setId(rs.getInt(1));
		          e.setVacationType(rs.getString(2));
		          e.setVacationStart(rs.getDate(3));
		          e.setVacationEnd(rs.getDate(4));
		          e.setVacationStatus(rs.getBoolean(5));
		          e.setUserId(rs.getInt(6));
		          
		          return e;
		      }
		  });
	}
	
	public List<UserVacation> findUserVacation(String date, String name, Integer departmentId){
		  String search = "";
		  if (date != null && date != "" && name != null && name != "") {
			  search = "where v.vacationStart >= " + date + "and (u.firstName like '%" + name + "%' or u.lastName like '%" + name + "%')";
		  }
		  else if (date != null && date != "") {
			  search = "where v.vacationStart >= '" + date + "'";
		  }
		  else if (name != null && name != "") {
			  search = "where (u.firstName like '%" + name + "%' or u.lastName like '%" + name + "%')";
		  }
		  
		  if (departmentId != null) {
			  if (search != "") {
				  search += " and u.departmentId = " + departmentId;
			  } else {
				  search = " where u.departmentId = " + departmentId;
			  }
		  }
		  
		  return template.query(""
		  		+ "select u.firstName, u.lastName, d.departmentName, u.userDur, u.vacBalance, v.vacationType, v.vacationStatus, v.vacationStart, v.vacationEnd " + 
				  "from user as u " +
				  "inner join vacation as v on v.userId = u.id " +
				  "inner join department as d on u.departmentId = d.Id " +
				  search, new RowMapper<UserVacation>(){
		      public UserVacation mapRow(ResultSet rs, int row) throws SQLException {
		    	  UserVacation uv = new UserVacation();
		          uv.setUserFullName(rs.getString(1) + " " + rs.getString(2));
		          uv.setUserDepartment(rs.getString(3));
		          uv.setUserDuration(rs.getString(4));
		          uv.setVacationBalance(rs.getInt(5));
		          uv.setVacationType(rs.getString(6));
		          uv.setVacationStatus(rs.getBoolean(7));
		          uv.setVacationStart(rs.getString(8));
		          uv.setVacationEnd(rs.getString(9));
		          
		          return uv;
		      }
		  });
		}
	
	public List<Department> getDepartments(){  
	    return template.query("select id,departmentName from department",new RowMapper<Department>(){  
	        public Department mapRow(ResultSet rs, int row) throws SQLException {  
	            Department department = new Department (); 
	            department.setId(rs.getInt(1)); 
	            department.setDepartmentName(rs.getString(2)); 
	            return department;  
	        }  
	    });  
	}
	 
	private Map<Long, String> usersToMap(List<User> userlist) {
	    	Map<Long,String> usersMap = new HashMap<>();
	    	
	    	for(User user : userlist) {
	    		usersMap.put(user.getId(),user.getUsername());
	    	}
	    	
	    	return usersMap;
	}
	
	public List<Vacation> getVacationTypes(){  
	    return template.query("select id,vacationType from vacation",new RowMapper<Vacation>(){  
	        public Vacation mapRow(ResultSet rs, int row) throws SQLException {  
	            Vacation vacationType = new Vacation (); 
	            vacationType.setId(rs.getInt(1)); 
	            vacationType.setVacationType(rs.getString(2)); 
	            return vacationType;  
	        }  
	    });  
	}
	
	
	
	
}  