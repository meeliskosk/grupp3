package com.hellokoding.account.service;

import com.hellokoding.account.model.Role;
import com.hellokoding.account.model.User;
import com.hellokoding.account.repository.RoleRepository;
import com.hellokoding.account.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

@Service
public class UserServiceImpl implements UserService {
   @Autowired
   private UserRepository userRepository;
   @Autowired
   private RoleRepository roleRepository;
   @Autowired
   private BCryptPasswordEncoder bCryptPasswordEncoder;

   @Override
   public void save(User user) {
       user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));

       List<Role> roles = new ArrayList<>();
       roles.add(roleRepository.findOne(user.getRoleId()));
       user.setRoles(new HashSet<>(roles));

       userRepository.save(user);
   }
   
   @Override
   public List<User> getAll(){
	   return userRepository.findAll();
   }

   @Override
   public User findByUsername(String username) {
       return userRepository.findByUsername(username);
   }

    @Override
    public boolean hasRole (String roleName)
   {
       return SecurityContextHolder.getContext().getAuthentication().getAuthorities().stream()
               .anyMatch(grantedAuthority -> grantedAuthority.getAuthority().equals(roleName));
   }
    @Override
	public User getLoggedInUser() {
		
		return findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
	}

    

}

