package com.hellokoding.account.service;

import java.util.List;
import java.util.Map;

import com.hellokoding.account.model.User;

public interface UserService {
    void save(User user);

    User findByUsername(String username);
    
    boolean hasRole(String roleName);
    
    User getLoggedInUser();
    
    List<User> getAll();
    
}
