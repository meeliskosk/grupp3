package com.hellokoding.account.model;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class Vacation {  
private int id;  
private String vacationType;
@DateTimeFormat (pattern = "yyyy-MM-dd")
private Date vacationStart;
@DateTimeFormat (pattern = "yyyy-MM-dd")
private Date vacationEnd;
private boolean vacationStatus;
private byte[] picture;
private long userId;
private String username;

public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getVacationType() {
	return vacationType;
}
public void setVacationType(String vacationType) {
	this.vacationType = vacationType;
}

public boolean isVacationStatus() {
	return vacationStatus;
}
public void setVacationStatus(boolean vacationStatus) {
	this.vacationStatus = vacationStatus;
}
public byte[] getPicture() {
	return picture;
}
public void setPicture(byte[] picture) {
	this.picture = picture;
}
public long getUserId() {
	return userId;
}
public void setUserId(long userId) {
	this.userId = userId;
}
public Date getVacationStart() {
	return vacationStart;
}
public void setVacationStart(Date vacationStart) {
	this.vacationStart = vacationStart;
}
public Date getVacationEnd() {
	return vacationEnd;
}
public void setVacationEnd(Date vacationEnd) {
	this.vacationEnd = vacationEnd;
}
public String getUsername() {
	return username;
}
public void setUsername(String username) {
	this.username = username;
}
  
}  