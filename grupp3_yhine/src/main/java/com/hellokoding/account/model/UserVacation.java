package com.hellokoding.account.model;  
  
public class UserVacation {  
	private String userFullName;
	private String userDepartment;
	private String userDuration;	
	private String vacationStart;
	private String vacationEnd;
	private String vacationType;
	private boolean vacationStatus;
	private int vacationBalance;
	
	
	public String getVacationType() {
		return vacationType;
	}
	public void setVacationType(String vacationType) {
		this.vacationType = vacationType;
	}
	public String getVacationStart() {
		return vacationStart;
	}
	public void setVacationStart(String vacationStart) {
		this.vacationStart = vacationStart;
	}
	public String getVacationEnd() {
		return vacationEnd;
	}
	public void setVacationEnd(String vacationEnd) {
		this.vacationEnd = vacationEnd;
	}
	public void setVacationStatus(boolean vacationStatus) {
		this.vacationStatus = vacationStatus;
	}
	public boolean getVacationStatus() {
		return vacationStatus;
	}
	public String getUserFullName() {
		return userFullName;
	}
	public void setUserFullName(String userFullName) {
		this.userFullName = userFullName;
	}
	public int getVacationBalance() {
		return vacationBalance;
	}
	public void setVacationBalance(int vacationBalance) {
		this.vacationBalance = vacationBalance;
	}
	public String getUserDepartment() {
		return userDepartment;
	}
	public void setUserDepartment(String userDepartment) {
		this.userDepartment = userDepartment;
	}
	public String getUserDuration() {
		return userDuration;
	}
	public void setUserDuration(String userDuration) {
		this.userDuration = userDuration;
	}
}  