package com.hellokoding.account.web;

import com.hellokoding.account.model.Department;
import com.hellokoding.account.model.Role;
import com.hellokoding.account.model.User;
import com.hellokoding.account.model.Vacation;
import com.hellokoding.account.repository.RoleRepository;
import com.hellokoding.account.repository.UserDao;
import com.hellokoding.account.service.SecurityService;
import com.hellokoding.account.service.UserService;
import com.hellokoding.account.validator.UserValidator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class UserController {
	
	@Autowired
	UserDao dao;
	
    @Autowired
    private UserService userService;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private UserValidator userValidator;
    
    @Autowired
    private RoleRepository roleRepository;

   
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model, String error, String logout) {
        if (error != null)
            model.addAttribute("error", "Your username and password is invalid.");

        if (logout != null)
            model.addAttribute("message", "You have been logged out successfully.");
        
       	return "/login";
    }
    
    @RequestMapping(value = {"/"}, method = RequestMethod.GET)
    public String welcome(Model model) {
    	
    	if(userService.hasRole("ROLE_MANAGER")) {
	    	return "redirect:/manager";
	    }
    	
    	if(userService.hasRole("ROLE_EMPLOYEE")) {
	    	return "redirect:/employee";
	    }
    	
    	if(userService.hasRole("ROLE_ACCOUNTANT")) {
	    	return "redirect:/accountant";
	    }
    	   	
    	return "redirect:/login";
    }
    

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String registration2(@ModelAttribute("userForm") User userForm, BindingResult bindingResult, Model model) {
        userValidator.validate(userForm, bindingResult);

        if (bindingResult.hasErrors()) {
            return "registration";
        }

        userService.save(userForm);

        securityService.autologin(userForm.getUsername(), userForm.getPasswordConfirm());
        
        if(userService.hasRole("ROLE_MANAGER")) {
	    	return "redirect:/manager";
	    }
    	
    	if(userService.hasRole("ROLE_EMPLOYEE")) {
	    	return "redirect:/employee";
	    }
    	
    	return "redirect:/accountant";   	   	        

    }
      
    private Map<Long, String> rolesToMap(List<Role> roles) {
        Map<Long, String> rolesMap = new HashMap<>();
        
        for(Role role : roles) {
            rolesMap.put(role.getId(), role.getName());
        }
        
        return rolesMap;
    }
    
    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registration(Model model) {
        model.addAttribute("userForm", new User());
        model.addAttribute("roles", rolesToMap(roleRepository.findAll()));
        model.addAttribute("departments", departmentsToMap(dao.getDepartments()));

        return "registration";
    }
    
    private Map<Integer, String> departmentsToMap(List<Department> departments) {
    	Map<Integer,String> departmentsMap = new HashMap<>();
    	
    	for(Department department : departments) {
    		departmentsMap.put(department.getId(),department.getDepartmentName());
    	}
    	
    	return departmentsMap;
    }
    
    private Map<String, String> vacationsToMap(List<Vacation> vacationTypes) {
    	Map<String, String> vacationTypeMap = new HashMap<>();
    	
    	for(Vacation vacation  : vacationTypes) {
    		vacationTypeMap.put(vacation.getVacationType(),vacation.getVacationType());
    	}
    	
    	return vacationTypeMap;
    }
    
    private Map<Long, String> usersToMap(List<User> userlist) {
    	Map<Long,String> usersMap = new HashMap<>();
    	
    	for(User user : userlist) {
    		usersMap.put(user.getId(),user.getUsername());
    	}
    	
    	return usersMap;
    }

    
    @RequestMapping(value = {"/manager"}, method = RequestMethod.GET)
    public String managerView(Model model) {

    	model.addAttribute("vacationTypes", vacationsToMap(dao.getVacationTypes()));
    	List<User> userlist=userService.getAll();  
        model.addAttribute("userlist", usersToMap(userlist));
        model.addAttribute("command", new Vacation());

        return "manager";
    }
    @RequestMapping(value = {"/employee"}, method = RequestMethod.GET)
    public String employeeView(Model model) {
    	model.addAttribute("vacationTypes", vacationsToMap(dao.getVacationTypes()));
        Vacation vacation = new Vacation();
        vacation.setUserId(userService.getLoggedInUser().getId());
        model.addAttribute("command", vacation);
    	return "employee";
    }
    
    
}

