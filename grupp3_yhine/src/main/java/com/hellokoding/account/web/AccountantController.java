package com.hellokoding.account.web;

import com.hellokoding.account.model.UserVacation;
import com.hellokoding.account.model.Role;
import com.hellokoding.account.model.User;
import com.hellokoding.account.repository.VacationDao;
import com.hellokoding.account.service.SecurityService;
import com.hellokoding.account.service.UserService;
import com.hellokoding.account.validator.UserValidator;

import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
public class AccountantController {
    @Autowired
    private UserService userService; 
    @Autowired
    private VacationDao vacDao; 
    
    @RequestMapping(value = {"/accountant"}, method = { RequestMethod.GET, RequestMethod.POST })
    public String accountant(Model model, 
    		@RequestParam(value = "date", required=false) String date, 
    		@RequestParam(value = "name", required=false) String name,
    		HttpServletRequest request) {
        List<User> userlist=userService.getAll();  
        model.addAttribute("userlist", userlist);
        
        List<UserVacation> vaclist = vacDao.findUserVacation(date, name, null);
        model.addAttribute("vaclist", vaclist);
	    model.addAttribute("message", request.getSession().getAttribute("message"));
	    
        return "accountant";
    }
}

