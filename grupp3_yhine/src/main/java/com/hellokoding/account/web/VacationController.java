package com.hellokoding.account.web;  

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;  
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;  
import org.springframework.web.bind.annotation.PathVariable;  
import org.springframework.web.bind.annotation.RequestMapping;  
import org.springframework.web.bind.annotation.RequestMethod;

import com.hellokoding.account.model.Department;
import com.hellokoding.account.model.User;
import com.hellokoding.account.model.UserVacation;
import com.hellokoding.account.model.Vacation;
import com.hellokoding.account.repository.UserDao;
import com.hellokoding.account.repository.VacationDao;
import com.hellokoding.account.service.UserService;

@Controller 
@RequestMapping("/vacation")  
public class VacationController {  
    @Autowired  
    VacationDao vacationDao;
    
    @Autowired  
    UserDao userDao;
    
    @Autowired
    private UserService userService;
      
     
    @RequestMapping("/vacForm")
    public String showform3(Model model){
        model.addAttribute("command", new Vacation());
        List<User> userlist=userService.getAll();
        model.addAttribute("userlist", usersToMap(userlist));
        model.addAttribute("departments", departmentsToMap(vacationDao.getDepartments()));
        
        return "vacform";
    }
    
    private Map<Long, String> usersToMap(List<User> userlist) {
    	Map<Long,String> usersMap = new HashMap<>();
    	
    	for(User user : userlist) {
    		usersMap.put(user.getId(),user.getUsername());
    	}
    	
    	return usersMap;
    }
    
    private Map<Integer, String> departmentsToMap(List<Department> departments) {
    	Map<Integer,String> departmentsMap = new HashMap<>();
    	
    	for(Department department : departments) {
    		departmentsMap.put(department.getId(),department.getDepartmentName());
    	}
    	
    	return departmentsMap;
    }
    
       
    @RequestMapping(value="/saveVacation",method = RequestMethod.POST)
    public String saveVacation(@ModelAttribute("vacation") Vacation vacation){
        vacationDao.saveVacation(vacation);
        return "redirect:/vacation/viewVacation";
    }
    
    @RequestMapping(value="/save1",method = RequestMethod.POST)
    public String save1(@ModelAttribute("vacation") Vacation vacation){
        vacationDao.saveVacation(vacation);
        return "redirect:/vacation/viewManVacation";
    }
           
    
    @RequestMapping(value = "/viewVacation", method = RequestMethod.GET)
    public String viewvac(Model m){
    	int departmentId = userService.getLoggedInUser().getDepartmentId();
        List<UserVacation> list= vacationDao.findUserVacation("", "", departmentId);
        m.addAttribute("list",list);
        return "viewvac";
   }
    
    @RequestMapping(value = "/viewManVacation", method = RequestMethod.GET)
    public String viewvac2(Model m){
    	List<Vacation> list= vacationDao.getVacation(userService.getLoggedInUser().getDepartmentId());
        m.addAttribute("list",list);
        return "viewvac2";
   }
        
    @RequestMapping(value="/editVacation/{id}")
    public String edit(@PathVariable int id, Model m){
        Vacation vacation=vacationDao.getVacationById(id);
        m.addAttribute("command",vacation);
        m.addAttribute("isManager", userService.hasRole("ROLE_MANAGER"));
        List<User> userlist=userService.getAll();  
        m.addAttribute("userlist", usersToMap(userlist));
        m.addAttribute("vacationTypes", vacationsToMap(vacationDao.getVacationTypes()));
        return "editvac";
   }
    private Map<String, String> vacationsToMap(List<Vacation> vacationTypes) {
    	Map<String, String> vacationTypeMap = new HashMap<>();
    	
    	for(Vacation vacation  : vacationTypes) {
    		vacationTypeMap.put(vacation.getVacationType(),vacation.getVacationType());
    	}
    	
    	return vacationTypeMap;
    }

    
    @RequestMapping(value="/editSave",method = RequestMethod.POST)
    public String editsave(@ModelAttribute("vacation") Vacation vacation){
        vacationDao.updateVacation(vacation);
       return "redirect:/vacation/viewManVacation";
    }
    
    @RequestMapping(value="/deleteVacation/{id}",method = RequestMethod.GET)
    public String delete(@PathVariable int id){
       vacationDao.delete(id);
        return "redirect:/vacation/viewManVacation";
    }
   
    
    @RequestMapping(value="/save",method = RequestMethod.POST)
    public String save(@ModelAttribute("vacation") Vacation vacation, HttpServletRequest request){
  	  if (userService.hasRole("ROLE_ACCOUNTANT")){
  		  vacation.setVacationStatus(true);
  		  
  		  request.getSession().removeAttribute("message");

  		  if (vacation.getVacationStart() == null || vacation.getVacationEnd() == null) {
  			  request.getSession().setAttribute("message", "Start and End dates should be filled");
  			  return "redirect:/accountant";
  		  }
  		  
  		  Calendar cal = Calendar.getInstance();
  		  cal.setTime(new Date());
  		  cal.add(Calendar.MONTH, -3);
  		  Date dateBefore3Months = cal.getTime();
  		
  		  if (vacation.getVacationStart().before(dateBefore3Months)) {
  			  request.getSession().setAttribute("message", "Date should not be older than 3 months");
  			  return "redirect:/accountant";
  		  }
  		  
  		  vacationDao.saveVacation(vacation);
  		  return "redirect:/accountant";
  	  }
        vacationDao.saveVacation(vacation);
        return "redirect:/employee";
    }
    
  
   
 
} 
