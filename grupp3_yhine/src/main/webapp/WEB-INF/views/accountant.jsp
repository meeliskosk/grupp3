
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Welcome, Accountant</title>
    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="container">
    <c:if test="${pageContext.request.userPrincipal.name != null}">
        <form id="logoutForm" method="POST" action="${contextPath}/logout">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </form>
        <h2>Accountant ${pageContext.request.userPrincipal.name} | <a onclick="document.forms['logoutForm'].submit()">Logout</a></h2>
    </c:if>
</div>
<div style="margin-left:15px">
<h2>Sisesta puhkuse kuup�evad:</h2>
<form class="form-group" style="width:200px" action="/account/vacation/save?${_csrf.parameterName}=${_csrf.token}" method = "POST">
  <label for="start">Algus:</label>
  <input class="form-control margin-bottom" type="date" name="vacationStart" id="start">
  <label for="end">L�pp:</label>
  <input class="form-control margin-bottom" type="date" name="vacationEnd" id="end">
  <label for="tyyp">Puhkuse t��p:</label>
  <select class="form-control margin-bottom" name="vacationType" id="tyyp">
    <option value="regular">Korraline puhkus</option>
    <option value="study">�ppepuhkus</option>
  </select>
  <label for="user">T��taja:</label>
  <select class="form-control margin-bottom" name="userId" id="user">
    <c:forEach var="user" items="${userlist}"> 
        <option value="${user.id}">${user.firstName} ${user.lastName}</option>
    </c:forEach>
  </select>
  <input id="confirmed" class="form-check-input margin-bottom" type="checkbox" checked disabled>
  <label class="form-check-label" for="confirmed">Kinnitatud</label>
  <br>
  <input type="submit" class="btn btn-success" value="Save" />
  <p style="color:red">${message}</p>
</form>

<br>
<br>
<form action="/account/accountant?${_csrf.parameterName}=${_csrf.token}" method = "POST">
    Search by start date: <input type="date" name="date">
    Search by name: <input type="text" name="name">
    <button>Search</button>
</form>
<h2>Puhkuste nimekiri</h2>
<table border="2" width="70%" cellpadding="2">
    <tr><th>Employee</th><th>Department</th><th>Duration</th><th>Start</th><th>End</th><th>Type</th><th>Confirmed</th><th>Vacation days left</th>
    <c:forEach var="vac" items="${vaclist}"> 
    <tr>
    <td>${vac.userFullName}</td>
    <td>${vac.userDepartment}</td>
    <td>${vac.userDuration}</td>
    <td>${vac.vacationStart}</td>
    <td>${vac.vacationEnd}</td>
    <td>${vac.vacationType}</td>
    <td>${vac.vacationStatus==true ? 'yes' : ''}</td>
    <td>${vac.vacationBalance}</td>
    </tr>
</c:forEach>
</table>
</div>
<!-- /container -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
</body>
</html>
