    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>    

	<h1>Edit vacation</h1>
      <form:form action="/account/vacation/editSave?${_csrf.parameterName}=${_csrf.token}" method = "POST">
  	 
         <form:hidden  path="id" />
        
         Vacation start :
          <form:input type= "date" path="vacationStart"  /><br>
         Vacation end :
          <form:input type= "date" path="vacationEnd"  /><br>
         
        
         <c:if test="${isManager}">
         
         Emloyee username:
         
  		<form:select path="userId" items="${userlist}" /><br>
  	
  		 </c:if>
   		
   		
	   	 Vacation Type :
	     <form:select path="vacationType" items="${vacationTypes}" /><br>
	 
	  	
	  	
          <c:if test="${isManager}">
        	
        	Kinnitatud :
        	<form:checkbox path="vacationStatus" /><br>
       
          </c:if>
          
           
          <input type="submit" value="Edit Save" /><br>  
         
  		
	</form:form> 
	<a href="/account">Back</a>
      
</body>
</html>